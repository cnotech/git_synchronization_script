# Git同步脚本

#### 介绍
此脚本于我们的Unity3D VR项目开始前期开发，方便使用第三方git同步服务的团队同步项目

此脚本不适合刚接触git的新人使用，不负责解决使用过程中的问题

#### 特性
1. 一键拉取，一键批注、增加、推送
2. 支持自动解决冲突并提供冲突解决高级功能
3. 自动检查git初始化并提供一键初始化功能

#### 运行条件

1. 支持Windows & Linux平台
2. 安装Git并添加至path，Git必须完成初始化设置
3. 脚本放置到项目文件夹根目录内
4. 项目只能有本地master分支和远程origin分支，并且已经默认设置了origin分支的远程服务器

#### 自行修改（均有注释提示）
1. 开头处的项目特征文件夹(Windows版)
2. push标签下的需要同步的文件夹