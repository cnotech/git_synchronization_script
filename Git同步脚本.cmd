@echo off
title Git Sync Batch
cd "%~dp0"
::在此处设置项目特征文件夹（替换Assets）
if not exist Assets (
    echo 请将脚本放置在项目文件夹内运行
    pause >nul
    exit
)
if not exist .git (
    echo 检测到您尚未进行初始化，按任意键完成初始化流程
    pause >nul
    git init
    cls
    goto reurl
)
:home
echo 请选择同步操作
echo 1.准备开搞     从服务器获取最新项目
echo 2.搞完了       把修改推送至服务器
echo 3.出现冲突     尝试解决冲突
echo 4.设置Origin   并尝试进行Push
set /p cho=
cls
if %cho%==1 goto pull
if %cho%==2 goto push
if %cho%==3 goto rebase
if %cho%==4 goto reurl
goto home


:pull
title 正在从服务器获取最新项目

echo ==================================
echo 正在跳转至master分支
echo ==================================
git checkout master

echo ==================================
echo 正在从服务器拉取项目（fetch）
echo ==================================
git fetch origin

echo ==================================
echo 正在尝试合并冲突（merge）
echo ==================================
git merge origin/master

title 同步完成
echo ==================================
echo 拉取完成，开搞！
echo ==================================
color 3f
pause >nul
exit

:push
title 即将提交更改至服务器
cls
echo.
echo 输入批注并回车即可挂机推送
set /p msage=请输入批注（留空则使用日期+时间）：
if not defined msage set msage=%date:~0,4%%date:~5,2%%date:~8,2%_%time:~0,2%%time:~3,2%%time:~6,2%%time:~9,2%


title 正在提交更改至服务器，不妨站起来运动运动-批注：%msage%
::在此处自定义需要同步的文件夹
git add Assets
git add Packages
git add ProjectSettings

echo ==================================
echo 正在批注更改（commit）
echo ==================================
git commit -m "%msage%"

echo ==================================
echo 正在跳转至master分支
echo ==================================
git checkout master

echo ==================================
echo 正在从服务器拉取项目（fetch）
echo ==================================
git fetch origin

echo ==================================
echo 正在尝试合并冲突（merge）
echo ==================================
git merge origin/master

echo ==================================
echo 正在向服务器推送更改（push）
echo ==================================
git push

title 同步完成
echo ==================================
echo 推送完成，师傅辛苦了
echo ==================================
color 3f
pause >nul
exit



:rebase
title 正在尝试解决冲突

echo 正在使用rebase参数拉取服务器端项目
git pull --rebase

title 操作完成
echo ==================================
echo 尝试解决完成，请根据提示操作
echo ==================================
color 3f
pause >nul
exit

:reurl
title 设置远程仓库
echo.
set /p origin=请右键粘贴远程仓库地址：
if exist readme.md goto skipTouch
echo ==================================
echo 正在使用ReadMe.md初始化（touch）
echo ==================================
echo Inited By GitSyncBatch >readme.txt
ren readme.txt readme.md
:skipTouch
git add readme.md
git commit -m "init"
echo ==================================
echo 正在设置Origin仓库（remote）
echo ==================================
git remote add origin %origin%
git remote set-url origin %origin%
echo ==================================
echo 正在尝试推送至Origin仓库（push）
echo ==================================
git push -u origin master
echo ==================================
echo 尝试推送完成
echo ==================================
color 3f
pause >nul
exit