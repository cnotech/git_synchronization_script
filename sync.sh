#!/bin/bash
pull() {
    echo ==================================
    echo 正在跳转至master分支
    echo ==================================
    git checkout master

    echo ==================================
    echo 正在从服务器拉取项目（fetch）
    echo ==================================
    git fetch origin

    echo ==================================
    echo 正在尝试合并冲突（merge）
    echo ==================================
    git merge origin/master

    echo ==================================
    echo 拉取完成，开搞！
    echo ==================================
    exit 1
}

push() {
    echo 即将提交更改至服务器
    read -p "请输入批注：" msage
    echo 正在提交更改至服务器，不妨站起来运动运动-批注：$msage
    ##在此处自定义需要同步的文件夹
    git add Assets
    git add Packages
    git add ProjectSettings

    echo ==================================
    echo 正在批注更改（commit）
    echo ==================================
    git commit -m "$msage"

    echo ==================================
    echo 正在跳转至master分支
    echo ==================================
    git checkout master

    echo ==================================
    echo 正在从服务器拉取项目（fetch）
    echo ==================================
    git fetch origin

    echo ==================================
    echo 正在尝试合并冲突（merge）
    echo ==================================
    git merge origin/master

    echo ==================================
    echo 正在向服务器推送更改（push）
    echo ==================================
    git push

    echo ==================================
    echo 推送完成，师傅辛苦了
    echo ==================================
    exit 1
}

rebase() {
    echo 正在使用rebase参数拉取服务器端项目
    git pull --rebase

    echo ==================================
    echo 尝试解决完成，请根据提示操作
    echo ==================================
    exit 1
}

reurl() {
    read -p "请粘贴远程仓库地址：" origin
    if  [ -f  "./README.md" ];then
    echo  "README.md存在"
    else
    echo ==================================
    echo 正在使用README.md初始化（touch）
    echo ==================================
    echo Inited By GitSyncBatch >readme.txt
    mv readme.txt README.md
    fi
    git add README.md
    git commit -m "init"
    echo ==================================
    echo 正在设置Origin仓库（remote）
    echo ==================================
    git remote add origin $origin
    git remote set-url origin $origin
    echo ==================================
    echo 正在尝试推送至Origin仓库（push）
    echo ==================================
    git push -u origin master
    echo ==================================
    echo 尝试推送完成
    echo ==================================
    exit 1
}

home(){
    echo 请选择同步操作
    echo 1.准备开搞     从服务器获取最新项目
    echo 2.搞完了       把修改推送至服务器
    echo 3.出现冲突     尝试解决冲突
    echo 4.设置Origin   并尝试进行Push
    read cho
    
    case "$cho" in 
        1)
        pull
        ;;
        2)
        push
        ;;
        3)
        rebase
        ;;
        4)
        reurl
        ;;
        *)
        home
        ;;
    esac
}


##入口
if  [ -d "./.git/" ];then
    home
else
    read -r -p "检测到您尚未进行初始化，是否需要执行初始化流程?[Y/n] " input
    case $input in
        [yY][eE][sS]|[yY])
	    	git init
            
            reurl
	    	;;
        *)
	    	exit 1
	    	;;
    esac
fi
